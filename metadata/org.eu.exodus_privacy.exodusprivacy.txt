Categories:Security
License:GPL-3.0-only
Web Site:https://news.exodus-privacy.eu.org/
Source Code:https://github.com/Exodus-Privacy/exodus-android-app
Issue Tracker:https://github.com/Exodus-Privacy/exodus-android-app/issues
Donate:https://exodus-privacy.eu.org/#help
Bitcoin:1exodusdyqXD81tS8SkcLhyFj9ioxWsaZ

Auto Name:Exodus Privacy

Repo Type:git
Repo:https://github.com/Exodus-Privacy/exodus-android-app.git

Build:1.0.3,3
    commit=release-v1.0.3
    subdir=app
    gradle=yes

Build:1.0.4,4
    commit=release-v1.0.4
    subdir=app
    gradle=yes

Build:1.1.0,5
    commit=release-v1.1.0
    subdir=app
    gradle=yes

Build:1.1.1,6
    commit=release-v1.1.1
    subdir=app
    gradle=yes

Build:1.2.0,7
    commit=release-v1.2.0
    subdir=app
    gradle=yes

Auto Update Mode:Version release-v%v
Update Check Mode:Tags
Current Version:1.2.0
Current Version Code:7
